
struct state;

int serialize(
	struct state* start_state,
	const char* path,
	bool should_write_header,
	const char* dfa_name,
	const char* trap_state_name);
