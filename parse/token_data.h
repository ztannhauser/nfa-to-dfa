
#include <wchar.h>

union token_data
{
	wchar_t* identifier;
	unsigned literal;
};

