
#include <stdio.h>
#include <avl.h>
#include <errno.h>

#include <error_codes.h>
#include <debug.h>

#include <memory/ginc.h>
#include <memory/gneeds.h>
#include <memory/delete.h>

#include <state/struct.h>
#include <state/new.h>
#include <state/add_transition.h>
#include <state/add_lambda_transition.h>

#include "../token.h"
#include "../token_data.h"

#include "../yylex/yylex.h"

#include "yyparse.h"

static int compare(const void* a, const void* b)
{
	return wcscmp( *((wchar_t**) a), *((wchar_t**) b));
}

int yyparse(int (*yybyte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar,
	enum token* token, union token_data* token_data, struct state** retval)
{
	int error = 0;
	ENTER;
	
	struct avl_tree_t* tree = avl_alloc_tree(compare, delete);
	
	dpv(tree);
	
	if (!tree)
		error = e_out_of_memory;
	
	int get_state(struct state** retval, wchar_t* name)
	{
		int error = 0;
		struct state* state;
		ENTER;
		
		struct avl_node_t* avl_state = avl_search(tree, &name);
		
		dpv(avl_state);
		
		if (avl_state)
		{
			*retval = ginc(avl_state->item);
		}
		else
		{
			// if you can't find it, make one
			error = new_state(&state, name);
			
			if (!avl_insert(tree, state))
				error = e_out_of_memory;
			
			if (!error)
				*retval = ginc(state);
		}
		
		EXIT;
		return error;
	}
	
	int read_transition(struct state* state)
	{
		int error = 0;
		bool is_ranged = false;
		bool is_lambda_transition = false;
		bool is_default_transition = false;
		unsigned literal, upto_literal;
		ENTER;
		
		// better be a literal:
		if (*token == t_literal)
		{
			literal = token_data->literal;
			error = yylex(yybyte, byte, wchar, token, token_data);
			
			if (!error && *token == t_ellipsis)
			{
				is_ranged = true;
				error = yylex(yybyte, byte, wchar, token, token_data);
				
				if (!error && *token != t_literal)
					error = e_syntax_error;
				
				if (!error)
					upto_literal = token_data->literal;
				
				if (!error)
					error = yylex(yybyte, byte, wchar, token, token_data);
			}
		}
		else if (*token == t_asterick)
		{
			is_default_transition = true;
			error = yylex(yybyte, byte, wchar, token, token_data);
		}
		else
		{
			is_lambda_transition = true;
		}
		
		// better be an '->'!
		if (!error && *token != t_arrow)
			error = e_syntax_error;
		
		// next?
		if (!error)
			error = yylex(yybyte, byte, wchar, token, token_data);
			
		// better be a identifier!
		if (!error && *token != t_identifier)
			error = e_syntax_error;
		
		wchar_t* identifier = NULL;
		
		struct state* transition_to = NULL;
		if (!error)
		{
			identifier = token_data->identifier;
			error = get_state(&transition_to, identifier);
		}
		
		if (!error)
		{
			if (is_default_transition)
			{
				delete(state->default_transition_to);
				state->default_transition_to = transition_to;
				error = gneeds(state, transition_to);
			}
			else if (is_ranged)
			{
				while (!error && literal - 1 <= upto_literal - 1)
					error = state_add_transition(state, literal++, transition_to);
			}
			else if (is_lambda_transition)
			{
				error = state_add_lambda_transition(state, transition_to);
			}
			else
			{
				error = state_add_transition(state, literal, transition_to);
			}
		}
		
		// next?
		if (!error)
			error = yylex(yybyte, byte, wchar, token, token_data);
		
		// might be a comma!
		if (!error && *token == t_comma)
			error = yylex(yybyte, byte, wchar, token, token_data);
		
		delete(transition_to);
		delete(identifier);
		
		EXIT;
		return error;
	}
	
	struct state* starting_state = NULL;
	
	int read_state()
	{
		int error = 0;
		struct state* state = NULL;
		bool is_starting_state = false;
		bool is_accepting = false;
		wchar_t* identifier = NULL;
		unsigned value = 0;
		ENTER;
		
		// read decorations:
		if (*token == t_arrow)
			is_starting_state = true,
			error = yylex(yybyte, byte, wchar, token, token_data);
		
		if (!error && *token == t_asterick)
			is_accepting = true,
			error = yylex(yybyte, byte, wchar, token, token_data);
		
		if (!error && *token == t_exclamation)
			value = 0,
			error = yylex(yybyte, byte, wchar, token, token_data);
		
		if (!error && *token == t_carrot)
			value = 2,
			error = yylex(yybyte, byte, wchar, token, token_data);
		
		if (!error && *token == t_pound)
		{
			error = yylex(yybyte, byte, wchar, token, token_data);
			
			// better be a literal:
			if (!error && *token != t_literal)
				error = e_syntax_error;
			
			if (!error)
				value = token_data->literal;
			
			error = yylex(yybyte, byte, wchar, token, token_data);
		}
		
		// identifier:
		if (!error && *token != t_identifier)
			error = e_syntax_error;
		
		// fetch state with this name
		if (!error)
		{
			identifier = token_data->identifier;
			error = get_state(&state, identifier);
		}
		
		dpv(identifier);
		dpv(error);
		
		dpv(state);
		
		// save starting state:
		if (is_starting_state)
		{
			delete(starting_state);
			starting_state = ginc(state);
		}
		
		
		// save accepting state:
		if (is_accepting)
			state->is_accepting = is_accepting;
		
		// save value:
		if (value)
			state->value = value;
		
		// moving on...
		if (!error)
			error = yylex(yybyte, byte, wchar, token, token_data);
		
		// expect colon:
		if (!error && *token != t_colon)
			error = e_syntax_error;
		
		// moving on...
		if (!error)
			error = yylex(yybyte, byte, wchar, token, token_data);
		
		while (!error && *token != t_semicolon)
			error = read_transition(state);
		
		// eat ';'
		if (!error)
			error = yylex(yybyte, byte, wchar, token, token_data);
		
		delete(state);
		delete(identifier);
		
		EXIT;
		return error;
	}
	
	dpv(error);
	
	while (!error && *token != t_EOF)
		error = read_state();
	
	if (error)
		CHECK;
	
	dpv(error);
	dpv(starting_state);
	
	if (!error && !starting_state)
	{
		fprintf(stderr, "starting state not specified!\n");
		error = e_syntax_error;
	}
	
	dpv(error);
	
	if (!error)
		*retval = ginc(starting_state);
	
	if (tree)
		avl_free_tree(tree);
	
	delete(starting_state);
	
	EXIT;
	return error;
}


















