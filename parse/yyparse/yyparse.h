
#include <inttypes.h>
#include <wchar.h>

enum token;
union token_data;
struct state;

int yyparse(int (*yybyte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar,
	enum token* token, union token_data* token_data, struct state** retval);
