
#undef DEBUGGING
#define DEBUGGING 0

#include <debug.h>

#include "header.h"
#include "ginc.h"

void* ginc(void* ptr)
{
	struct gmemory_header* header;
	ENTER;
	
	if (ptr)
	{
		header = ptr - sizeof(*header);
		header->refcount++;
		dpv(header->refcount);
	}
	
	EXIT;
	return ptr;
}


