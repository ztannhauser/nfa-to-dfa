
#undef DEBUGGING
#define DEBUGGING 0

#include <stdbool.h>

#include <debug.h>

#include "header.h"
#include "gnoneed.h"
#include "gconsider.h"

static bool are_nonzero_refcounts_upstream(struct gmemory_header* header)
{
	bool retval = false;
	size_t i, n;
	ENTER;
	
	dpv(header);
	
	switch (header->status)
	{
		case s_default:
		{
			dpv(header->refcount);
			
			if (header->refcount > 0)
				retval = true;
			
			dpv(header->thoseineed.n);
			dpv(header->thosewhoneedme.n);
			
			header->status = s_searching;
			
			for (i = 0, n = header->thosewhoneedme.n; !retval && i < n; i++)
			{
				retval = are_nonzero_refcounts_upstream(header->thosewhoneedme.headers[i]);
			}
			
			header->status = s_default;
			break;
		}
		
		case s_deleting:
			retval = true;
			break;
		
		case s_searching:
			retval = false;
			break;
	}
	
	EXIT;
	return retval;
}

void gconsider(struct gmemory_header* header)
{
	bool should_free;
	size_t i, n;
	void* ptr;
	ENTER;
	
	should_free = !are_nonzero_refcounts_upstream(header);
	
	dpvb(should_free);
	
	if (should_free)
	{
		ptr = header + 1;
		
		header->status = s_deleting;
		
		dpv(header->thosewhoneedme.n);
		dpv(header->thoseineed.n);
		for (i = 0, n = header->thoseineed.n; i < n; i++)
		{
			gnoneed(ptr, header->thoseineed.headers[0] + 1);
		}
		
		if (header->deconstructor)
			header->deconstructor(ptr);
		
		free(header->thoseineed.headers);
		free(header->thosewhoneedme.headers);
		free(header);
	}
	
	EXIT;
}































