
#include <stdlib.h>

struct gmemory_header
{
	unsigned refcount;
	size_t size;
	void (*deconstructor)(void*);
	struct {
		struct gmemory_header** headers;
		size_t n, cap;
	} thoseineed, thosewhoneedme;
	enum {
		s_default,
		s_deleting,
		s_searching,
	} status;
};

