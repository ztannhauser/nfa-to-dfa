
#undef DEBUGGING
#define DEBUGGING 0

#include <stdbool.h>

#include <debug.h>

#include "header.h"

#include "gconsider.h"
#include "gnoneed.h"

void gnoneed(void* nolongerinneed, void* needsmenolonger)
{
	struct gmemory_header* nlin_header;
	struct gmemory_header* nmnl_header;
	size_t i, n;
	bool found1, found2;
	ENTER;
	
	dpv(nolongerinneed);
	dpv(needsmenolonger);
	
	nlin_header = nolongerinneed  - sizeof(*nlin_header);
	nmnl_header = needsmenolonger - sizeof(*nmnl_header);
	
	// remove from 'nlin_header->thoseineed':
	{
		dpv(nlin_header->thoseineed.n);
		
		for (found1 = false, i = 0, n = nlin_header->thoseineed.n; i < n; i++)
		{
			if (found1)
			{
				nlin_header->thoseineed.headers[i - 1] = nlin_header->thoseineed.headers[i];
			}
			else if (nlin_header->thoseineed.headers[i] == nmnl_header)
			{
				found1 = true;
			}
		}
		
		assert(found1);
		
		if (found1)
			nlin_header->thoseineed.n--;
		
		dpv(nlin_header->thoseineed.n);
	}
	
	// remove from 'nmnl_header->thosewhoneedme':
	{
		dpv(nmnl_header->thosewhoneedme.n);
		
		for (found2 = false, i = 0, n = nmnl_header->thosewhoneedme.n; i < n; i++)
		{
			if (found2)
			{
				nmnl_header->thosewhoneedme.headers[i - 1] = nmnl_header->thosewhoneedme.headers[i];
			}
			else if (nmnl_header->thosewhoneedme.headers[i] == nlin_header)
			{
				found2 = true;
			}
		}
		
		assert(found2);
		
		if (found2)
			nmnl_header->thosewhoneedme.n--;
		
		dpv(nmnl_header->thosewhoneedme.n);
	}
	
	assert(found1 == found2);
	
	if (found1)
		gconsider(nmnl_header);
	
	EXIT;
}






















