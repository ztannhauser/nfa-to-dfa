

#include <wchar.h>
#include <stdbool.h>
#include <inttypes.h>


#ifndef _DFA_TUPLE_
#define _DFA_TUPLE_
struct dfa_tuple {
	// A Deterministic Finite Automaton is defined as a 5-tuple (Q, Σ, δ, s, F)
	unsigned max_state;    // Q = {0 ... max_state}
	unsigned max_alphabet; // Σ = {0 ... max_alphabet}
	unsigned **table;      // δ = (state, letter) -> (state)
	unsigned start;        // s = starting_state
	bool *accepting;       // bool accepting[state] = {q ∈ F | q ∈ Q}
	
	unsigned *values;      // unsigned values[state];
	const char **names;    // const wchar_t* names[state];
};
#endif // ifndef _DFA_TUPLE_


extern struct dfa_tuple converted_dfa;


struct dfa_tuple converted_dfa = {
	.max_state = 14,
	
	.max_alphabet = 99,
	
	.table = (unsigned**) &(unsigned*[]) {
		[0]     = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 0}, // trap state
		[0 + 1] = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 13 + 1, [65] = 1 + 1, [66] = 8 + 1, [67] = 11 + 1, },
		[1 + 1] = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 3 + 1, [65] = 2 + 1, [98] = 4 + 1, [99] = 6 + 1, },
		[2 + 1] = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 3 + 1, [65] = 2 + 1, },
		[3 + 1] = (unsigned*) &(unsigned[]) {[97] = 3 + 1, },
		[4 + 1] = (unsigned*) &(unsigned[]) {[97] = 3 + 1, [98] = 5 + 1, },
		[5 + 1] = (unsigned*) &(unsigned[]) {[98] = 5 + 1, },
		[6 + 1] = (unsigned*) &(unsigned[]) {[97] = 3 + 1, [99] = 7 + 1, },
		[7 + 1] = (unsigned*) &(unsigned[]) {[99] = 7 + 1, },
		[8 + 1] = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 5 + 1, [66] = 9 + 1, [97] = 4 + 1, [99] = 10 + 1, },
		[9 + 1] = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 5 + 1, [66] = 9 + 1, },
		[10 + 1] = (unsigned*) &(unsigned[]) {[98] = 5 + 1, [99] = 7 + 1, },
		[11 + 1] = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 7 + 1, [67] = 12 + 1, [97] = 6 + 1, [98] = 10 + 1, },
		[12 + 1] = (unsigned*) &(unsigned[]) {[0 ... 99 + 1] = 7 + 1, [67] = 12 + 1, },
		[13 + 1] = (unsigned*) &(unsigned[]) {[97] = 3 + 1, [98] = 5 + 1, [99] = 7 + 1, },
	},
	
	.start = 0 + 1,
	
	.accepting = (bool*) &(bool[]) {
		[0] = false, // trap state
		[1 + 1] = true,
		[3 + 1] = true,
		[4 + 1] = true,
		[5 + 1] = true,
		[6 + 1] = true,
		[7 + 1] = true,
		[8 + 1] = true,
		[10 + 1] = true,
		[11 + 1] = true,
		[13 + 1] = true,
	},
	
	.values = (unsigned*) &(unsigned[]) {
		[1 ... 14] = 1,
	},
	
	.names = (const char**) &(const char*[]) {
		[0 + 1] = "{start,A₁,B₁,C₁}",
		[1 + 1] = "{A₁,B₂,C₂}",
		[2 + 1] = "{A₁}",
		[3 + 1] = "{A₂}",
		[4 + 1] = "{A₂,B₂}",
		[5 + 1] = "{B₂}",
		[6 + 1] = "{A₂,C₂}",
		[7 + 1] = "{C₂}",
		[8 + 1] = "{B₁,A₂,C₂}",
		[9 + 1] = "{B₁}",
		[10 + 1] = "{B₂,C₂}",
		[11 + 1] = "{C₁,A₂,B₂}",
		[12 + 1] = "{C₁}",
		[13 + 1] = "{A₂,B₂,C₂}",
	}
};



