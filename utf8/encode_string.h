
#include <wchar.h>
#include <inttypes.h>

void utf8_encode_string(uint8_t* utf8_out, wchar_t* unicode_in);
