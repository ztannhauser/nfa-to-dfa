
#include <error_codes.h>
#include <debug.h>

#include "decode.h"

int utf8_decode(wchar_t* retval, uint8_t buffer[6])
{
	int error = 0;
	ENTER;
	
	dpv(buffer[0]);
	
	switch (buffer[0])
	{
		case 0b00000000 ... 0b01111111:
			*retval = buffer[0];
			break;
		
		case 0b11000000 ... 0b11011111:
			*retval =
				  ((0b00011111 & buffer[0]) << 6)
				| ((0b00111111 & buffer[1]) << 0);
			break;
		
		case 0b11100000 ... 0b11101111:
			*retval =
				  ((0b00001111 & buffer[0]) << 12)
				| ((0b00111111 & buffer[1]) << 6)
				| ((0b00111111 & buffer[2]) << 0);
			break;
		
		case 0b11110000 ... 0b11110111:
			*retval =
				  ((0b00000111 & buffer[0]) << 18)
				| ((0b00111111 & buffer[1]) << 12)
				| ((0b00111111 & buffer[2]) << 6)
				| ((0b00111111 & buffer[3]) << 0);
			break;
		
		case 0b11111000 ... 0b11111011:
			*retval =
				  ((0b00000011 & buffer[0]) << 24)
				| ((0b00111111 & buffer[1]) << 18)
				| ((0b00111111 & buffer[2]) << 12)
				| ((0b00111111 & buffer[3]) << 6)
				| ((0b00111111 & buffer[4]) << 0);
			break;
		
		case 0b11111100 ... 0b11111101:
			*retval =
				  ((0b00000001 & buffer[0]) << 30)
				| ((0b00111111 & buffer[1]) << 24)
				| ((0b00111111 & buffer[2]) << 18)
				| ((0b00111111 & buffer[3]) << 12)
				| ((0b00111111 & buffer[4]) << 6)
				| ((0b00111111 & buffer[5]) << 0);
			break;
		
		default: error = e_malformed_utf8;
	}
	
	dpv(error);
	dpv(*retval);
	
	EXIT;
	return error;
}









