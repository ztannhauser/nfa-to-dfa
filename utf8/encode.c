
#include <stdlib.h>

#include <debug.h>

#include "encode.h"

int utf8_encode(uint8_t* utf8_out, wchar_t unicode)
{
	switch (unicode)
	{
		case 0x00000000 ... 0x0000007F:
			*utf8_out++ = 0b00000000 | (0b01111111 & (unicode >>  0));
			return 1;
		case 0x00000080 ... 0x000007FF:
			*utf8_out++ = 0b11000000 | (0b00011111 & (unicode >>  6));
			*utf8_out++ = 0b10000000 | (0b00111111 & (unicode >>  0));
			return 2;
		case 0x00000800 ... 0x0000FFFF:
			*utf8_out++ = 0b11100000 | (0b00001111 & (unicode >> 12));
			*utf8_out++ = 0b10000000 | (0b00111111 & (unicode >>  6));
			*utf8_out++ = 0b10000000 | (0b00111111 & (unicode >>  0));
			return 3;
		case 0x00010000 ... 0x001FFFFF:
			*utf8_out++ = 0b11110000 | (0b00000111 & (unicode >> 18));
			*utf8_out++ = 0b10000000 | (0b00111111 & (unicode >> 12));
			*utf8_out++ = 0b10000000 | (0b00111111 & (unicode >>  6));
			*utf8_out++ = 0b10000000 | (0b00111111 & (unicode >>  0));
			return 4;
		case 0x00200000 ... 0x03FFFFFF:
			*utf8_out++ = 0b11111000 | (0b00000011 & (unicode >> 24));
			*utf8_out++ = 0b10000000 | (0b00111111 & (unicode >> 18));
			*utf8_out++ = 0b10000000 | (0b00111111 & (unicode >> 12));
			*utf8_out++ = 0b10000000 | (0b00111111 & (unicode >>  6));
			*utf8_out++ = 0b10000000 | (0b00111111 & (unicode >>  0));
			return 5;
		case 0x04000000 ... 0x7FFFFFFF:
			*utf8_out++ = 0b11111100 | (0b00000001 & (unicode >> 30));
			*utf8_out++ = 0b10000000 | (0b00111111 & (unicode >> 24));
			*utf8_out++ = 0b10000000 | (0b00111111 & (unicode >> 18));
			*utf8_out++ = 0b10000000 | (0b00111111 & (unicode >> 12));
			*utf8_out++ = 0b10000000 | (0b00111111 & (unicode >>  6));
			*utf8_out++ = 0b10000000 | (0b00111111 & (unicode >>  0));
			return 6;
	}
	
	abort();
}












