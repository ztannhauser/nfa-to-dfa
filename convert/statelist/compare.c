
#include <debug.h>

#include <macros/cmp.h>

#include "compare.h"

int compare_states(const void* a, const void* b)
{
	return cmp(((struct state**) a)[0], ((struct state**) b)[0]);
}

