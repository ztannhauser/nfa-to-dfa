
#include <debug.h>

#include "struct.h"
#include "compare.h"
#include "contains.h"

bool statelist_contains(struct statelist* this, struct state* state)
{
/*	dpv(bsearch(&state, this->states, this->n, sizeof(*this->states), compare));*/
	return !!bsearch(&state, this->states, this->n, sizeof(*this->states), compare_states);
}

