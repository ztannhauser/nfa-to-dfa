
#include <debug.h>

#include <memory/grealloc.h>
#include <memory/gneeds.h>

#include "compare.h"
#include "struct.h"
#include "contains.h"
#include "insert.h"

int statelist_insert(struct statelist* this, struct state* state)
{
	int error = 0;
	ENTER;
	
	if (!statelist_contains(this, state))
	{
		if (this->n + 1 >= this->cap)
		{
			error = grealloc((void**) &this->states, sizeof(*this->states) * (this->cap = this->cap * 2 ?: 1));
		}
		
		if (!error)
			error = gneeds(this, state);
		
		if (!error)
		{
			size_t n = this->n - 1;
			
			for (; n + 1 >= 0 + 1 && compare_states(&state, &this->states[n]) < 0; n--)
				this->states[n + 1] = this->states[n];
			
			dpv(n + 1);
			
			this->states[n + 1] = state, this->n++;
		}
	}
	
	EXIT;
	return error;
}


