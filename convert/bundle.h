
struct state_list;
struct state;

struct bundle
{
	struct statelist* statelist; // must be first
	struct state* combined;
};
