
#include <debug.h>

#include <state/struct.h>

#include "statelist/struct.h"
#include "statelist/insert.h"
#include "statelist/contains.h"

#include "push_lambda_transitions.h"

int push_lambda_transitions(struct statelist* statelist)
{
	int error = 0;
	ENTER;
	
	bool again;
	size_t i, n;
	struct state *state1, *state2;
	size_t j, m;
	
	do for (again = false, i = 0, n = statelist->n; !error && i < n; i++)
	{
		state1 = statelist->states[i];
		
		for (j = 0, m = state1->lambda_transitions.n; !error && j < m; j++)
		{
			state2 = state1->lambda_transitions.data[j];
			
			if (!statelist_contains(statelist, state2))
			{
				dprintf("adding %p\n", state2);
				error = statelist_insert(statelist, state2);
				again = true;
			}
		}
	} while (!error && again);
	
	EXIT;
	return error;
}






















