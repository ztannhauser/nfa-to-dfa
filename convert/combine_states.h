
struct state;
struct avl_tree_t;
struct statelist;

int combine_states(struct avl_tree_t* tree, struct statelist* statelist, struct state** retval);
