
#include <debug.h>

#include <macros/min.h>
#include <macros/cmp.h>

#include "statelist/struct.h"

#include "compare_bundles.h"

int compare_bundles(const void* a, const void* b)
{
	size_t i, n;
	struct statelist * const *A = a, * const *B = b;
	
	if (*A == *B)
		return 0;
	
	for (i = 0, n = min((*A)->n, (*B)->n); i < n; i++)
		if ((*A)->states[i] != (*B)->states[i])
			return cmp((*A)->states[i], (*B)->states[i]);
	
	return cmp((*A)->n, (*B)->n);
}

