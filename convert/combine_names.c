
#include <debug.h>

#include <memory/grealloc.h>
#include <memory/ginc.h>
#include <memory/delete.h>

#include <state/struct.h>

#include "statelist/struct.h"

#include "combine_names.h"

int combine_names(struct statelist* statelist, wchar_t** retval)
{
	int error = 0;
	ENTER;
	
	size_t i, n;
/*	struct state* state;*/
	struct { wchar_t* wchars; size_t n, cap; } name = {NULL, 0, 0};
	
	int push(const wchar_t wc)
	{
		int error = 0;
		
		if (name.n + 1 >= name.cap)
			error = grealloc((void**) &name.wchars, sizeof(*name.wchars) * (name.cap = name.cap * 2 ?: 1));
		
		if (!error)
			name.wchars[name.n++] = wc;
		
		return error;
	}
	
	error = push(L'{');
	
	wchar_t* statename;
	for (i = 0, n = statelist->n; !error && i < n; i++)
	{
		for (statename = statelist->states[i]->name; !error && *statename;)
			error = push(*statename++);
		
		if (!error && i + 1 < n)
			error = push(L',');
	}
	
	if (!error)
		error = push(L'}');
	
	if (!error)
		error = push(L'\0');
	
	if (!error)
		*retval = ginc(name.wchars);
	
	delete(name.wchars);
	
	EXIT;
	return error;
}
















