

TARGET = release

CC = gcc

CPPFLAGS += -I .

ifeq ($(TARGET), release)
CPPFLAGS += -D DEBUGGING=0
else
CPPFLAGS += -D DEBUGGING=1
endif

CFLAGS += -Wall -Werror

ifeq ($(TARGET), release)
CFLAGS += -O2
CFLAGS += -flto
else
CFLAGS += -g
CFLAGS += -Wno-unused-variable
CFLAGS += -Wno-unused-function
CFLAGS += -Wno-unused-but-set-variable
endif

LDLIBS += -lavl

default: bin/nfa-to-dfa.$(TARGET)

bin:
	rm -f ./bin
	ln -s `mktemp -d` bin
	find -type d | sed 's ^ bin/ ' | xargs -d \\n mkdir -p

bin/srclist.mk: | bin
	find -name '*.c' ! -name '*-out.c' | sed 's/^/SRCS += /g' > $@

include bin/srclist.mk

OBJS = $(patsubst %.c,bin/%.$(TARGET).o,$(SRCS))
DEPS = $(patsubst %.c,bin/%.$(TARGET).d,$(SRCS))

ARGS += ./example1.nfa -o ./example1-out.c
#ARGS += ./example2.nfa -o ./example2-out.c
#ARGS += ./example3.nfa -o ./example3-out.c -t trap
#ARGS += ./example4.nfa -o ./example4-out.c
#ARGS += ./example5.nfa -o ./example5-out.c

run: bin/nfa-to-dfa.$(TARGET)
	$< $(ARGS)

valrun: bin/nfa-to-dfa.$(TARGET)
	valgrind $< $(ARGS)

valrun-leak: bin/nfa-to-dfa.$(TARGET)
	valgrind --leak-check=full $< $(ARGS)

outs: example1-out.c example2-out.c example3-out.c

%-out.c: bin/nfa-to-dfa.$(TARGET) %.nfa 
	$^ -o $@

install: ~/bin/nfa-to-dfa

~/bin/nfa-to-dfa: bin/nfa-to-dfa.release
	cp -vf $< $@

#/tmp/digraph.png: /tmp/digraph.gv
#	dot /tmp/digraph.gv -Tpng > /tmp/digraph.png

bin/nfa-to-dfa.$(TARGET): $(OBJS)
	$(CC) $(LDFLAGS) $^ $(LOADLIBES) $(LDLIBS) -o $@

bin/%.$(TARGET).o bin/%.$(TARGET).d: %.c
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -MMD -o bin/$*.$(TARGET).o || (gedit $<; false)

include $(DEPS)


















