

#include <wchar.h>
#include <stdbool.h>
#include <inttypes.h>


#ifndef _DFA_TUPLE_
#define _DFA_TUPLE_
struct dfa_tuple {
	// A Deterministic Finite Automaton is defined as a 5-tuple (Q, Σ, δ, s, F)
	unsigned max_state;    // Q = {0 ... max_state}
	unsigned max_alphabet; // Σ = {0 ... max_alphabet}
	unsigned **table;      // δ = (state, letter) -> (state)
	unsigned start;        // s = starting_state
	bool *accepting;       // bool accepting[state] = {q ∈ F | q ∈ Q}
	
	unsigned *values;      // unsigned values[state];
	const char **names;    // const wchar_t* names[state];
};
#endif // ifndef _DFA_TUPLE_


extern struct dfa_tuple converted_dfa;


struct dfa_tuple converted_dfa = {
	.max_state = 12,
	
	.max_alphabet = 98,
	
	.table = (unsigned**) &(unsigned*[]) {
		// [0] = {}, // trap state
		[0 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 11 + 1, [97] = 1 + 1, [98] = 10 + 1, },
		[1 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 8 + 1, [97] = 2 + 1, [98] = 4 + 1, },
		[2 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 9 + 1, [97] = 3 + 1, [98] = 5 + 1, },
		[3 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 8 + 1, [97] = 2 + 1, [98] = 4 + 1, },
		[4 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 9 + 1, [97] = 5 + 1, [98] = 7 + 1, },
		[5 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 8 + 1, [97] = 4 + 1, [98] = 6 + 1, },
		[6 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 9 + 1, [97] = 7 + 1, },
		[7 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 8 + 1, [97] = 6 + 1, },
		[8 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 9 + 1, },
		[9 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 8 + 1, },
		[10 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 8 + 1, [97] = 4 + 1, [98] = 6 + 1, },
		[11 + 1] = (unsigned*) &(unsigned[]) {[0 ... 98 + 1] = 8 + 1, },
	},
	
	.start = 0 + 1,
	
	.accepting = (bool*) &(bool[]) {
		[0] = false, // trap state
		[1 + 1] = true,
		[3 + 1] = true,
		[5 + 1] = true,
		[6 + 1] = true,
		[7 + 1] = true,
		[9 + 1] = true,
		[10 + 1] = true,
		[11 + 1] = true,
	},
	
	.values = (unsigned*) &(unsigned[]) {
		[1 ... 12 + 1] = 1,
		[1 + 1] = 3,
		[3 + 1] = 3,
		[5 + 1] = 3,
		[6 + 1] = 2,
		[7 + 1] = 6,
		[9 + 1] = 3,
		[10 + 1] = 3,
		[11 + 1] = 3,
	},
	
	.names = (const char**) &(const char*[]) {
		[0 + 1] = "{start,₀,₆,₁}",
		[1 + 1] = "{₀,₁,f,₇,₈,₁₁}",
		[2 + 1] = "{₀,₁,₉}",
		[3 + 1] = "{₀,₁,f,₇,₈,₁₁,₁₀}",
		[4 + 1] = "{₂,₃,₉}",
		[5 + 1] = "{₂,₃,f,₇,₈,₁₁,₁₀}",
		[6 + 1] = "{₄,₅,f,₉}",
		[7 + 1] = "{₄,₅,f,₇,₈,₁₁,₁₀}",
		[8 + 1] = "{₉}",
		[9 + 1] = "{f,₇,₈,₁₁,₁₀}",
		[10 + 1] = "{₂,₃,f,₇,₈,₁₁}",
		[11 + 1] = "{f,₇,₈,₁₁}",
	}
};



