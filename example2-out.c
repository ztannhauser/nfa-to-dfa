

#include <wchar.h>
#include <stdbool.h>
#include <inttypes.h>


#ifndef _DFA_TUPLE_
#define _DFA_TUPLE_
struct dfa_tuple {
	// A Deterministic Finite Automaton is defined as a 5-tuple (Q, Σ, δ, s, F)
	unsigned max_state;    // Q = {0 ... max_state}
	unsigned max_alphabet; // Σ = {0 ... max_alphabet}
	unsigned **table;      // δ = (state, letter) -> (state)
	unsigned start;        // s = starting_state
	bool *accepting;       // bool accepting[state] = {q ∈ F | q ∈ Q}
	
	unsigned *values;      // unsigned values[state];
	const char **names;    // const wchar_t* names[state];
};
#endif // ifndef _DFA_TUPLE_


extern struct dfa_tuple converted_dfa;


struct dfa_tuple converted_dfa = {
	.max_state = 7,
	
	.max_alphabet = 69,
	
	.table = (unsigned**) &(unsigned*[]) {
		[0]     = (unsigned*) &(unsigned[]) {[0 ... 69 + 1] = 0}, // trap state
		[0 + 1] = (unsigned*) &(unsigned[]) {[65] = 1 + 1, },
		[1 + 1] = (unsigned*) &(unsigned[]) {[66] = 2 + 1, [67] = 5 + 1, },
		[2 + 1] = (unsigned*) &(unsigned[]) {[66] = 2 + 1, [67] = 3 + 1, [69] = 4 + 1, },
		[3 + 1] = (unsigned*) &(unsigned[]) {[67] = 3 + 1, },
		[4 + 1] = (unsigned*) &(unsigned[]) {},
		[5 + 1] = (unsigned*) &(unsigned[]) {[66] = 6 + 1, [67] = 5 + 1, },
		[6 + 1] = (unsigned*) &(unsigned[]) {[66] = 6 + 1, [69] = 4 + 1, },
	},
	
	.start = 0 + 1,
	
	.accepting = (bool*) &(bool[]) {
		[0] = false, // trap state
		[3 + 1] = true,
		[4 + 1] = true,
		[5 + 1] = true,
	},
	
	.values = (unsigned*) &(unsigned[]) {
		[1 ... 7] = 1,
	},
	
	.names = (const char**) &(const char*[]) {
		[0 + 1] = "{start,₀,₅}",
		[1 + 1] = "{₁,₂,₆,₇}",
		[2 + 1] = "{₁,₂,₈,₉}",
		[3 + 1] = "{₃,₄,final}",
		[4 + 1] = "{₁₀,final}",
		[5 + 1] = "{₃,₄,₆,₇,final}",
		[6 + 1] = "{₈,₉}",
	}
};



